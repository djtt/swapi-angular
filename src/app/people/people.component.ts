import { Component, OnInit } from '@angular/core';
import { PeopleList } from '../peopleList';

import { FilmService } from '../film.service';
import { PeopleService } from '../people.service';
import { DataStorageService } from '../data-storage.service';
import { FilmList } from '../filmList';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {
  peopleList: PeopleList;
  filmList: FilmList;
  private toPassUrl: string;

  constructor(
    private peopleService: PeopleService,
    private dataStorageService: DataStorageService,
    private filmService: FilmService) { }

  ngOnInit() {
    this.getPeople();
  }

  getPeople(passedUrl?: string): void {
    if (passedUrl != null) {
      this.toPassUrl = passedUrl;
    }
    this.peopleList = this.dataStorageService.getPeopleList();
    if (this.peopleList == null || this.toPassUrl) {
      this.peopleService.getAllPeople(this.toPassUrl).subscribe(peopleList2 => {
        this.dataStorageService.setPeopleList(peopleList2);
        this.peopleList = this.dataStorageService.getPeopleList();
        this.getFilms()});
    }
  }
  getFilms(): void {
    this.filmList = this.dataStorageService.getFilmList();
    if (!this.filmList) {
      this.filmService.getAllFilms().subscribe(films => {this.dataStorageService.setFilmList(films)});
    }
  }

}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { PeopleModel } from './peopleModel';
import { PeopleList } from './peopleList';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
providedIn: 'root'})

export class PeopleService {
private peopleUrl = 'https://test.djtt.nl/api/people'; // Url to webapi
private url: string;

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getAllPeople(newUrl?: string): Observable<PeopleList> {
    if (!newUrl) {
      newUrl = this.peopleUrl;
    }
    return this.http.get<PeopleList>(newUrl)
      .pipe(
      tap(_ => this.log('fetched all heroes')),
      catchError(this.handleError<PeopleList>('getPeople'))
      );
  }

  getPeople(id: number): Observable<PeopleModel> {
    const url = `${this.peopleUrl}/${id}`;
    return this.http.get<PeopleModel>(url).pipe(
      tap(_ => this.log(`fetched actor id=${id}`)),
      catchError(this.handleError<PeopleModel>(`getPeople id=${id}`))
    );
  }

  searchPeople(term: string): Observable<PeopleModel[]> {
    if (!term.trim()) {
      // if not search term, return empty array.
      return of([]);
    }
    return this.http.get<PeopleModel[]>(`${this.peopleUrl}/?name=${term}`).pipe(
      tap(_ => this.log(`found heroes matching "${term}"`)),
      catchError(this.handleError<PeopleModel[]>('searchPeople', []))
    );
  }
   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */ 
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the eroor to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      // let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  /** Log a PeopleService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`PeopleService: ${message}`);
  }
}

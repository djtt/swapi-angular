import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { PeopleModel } from '../peopleModel';
import { PeopleService } from '../people.service';

@Component({
  selector: 'app-hero-search',
  templateUrl: './people-search.component.html',
  styleUrls: [ './people-search.component.css' ]
})
export class PeopleSearchComponent implements OnInit {
  people$: Observable<PeopleModel[]>;
  private searchTerms = new Subject<string>();

  constructor(private peopleService: PeopleService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.people$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.peopleService.searchPeople(term)),
    );
  }
}
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { FilmList } from './filmList';
import { FilmModel } from './filmModel';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
providedIn: 'root'})

export class FilmService {
private filmUrl = 'https://test.djtt.nl/api/films'; // Url to webapi

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getAllFilms(): Observable<FilmList> {
    return this.http.get<FilmList>(this.filmUrl)
      .pipe(
      tap(_ => this.log('fetched all films')),
      catchError(this.handleError<FilmList>('getFilm'))
      );
  }

  getFilm(id: number): Observable<FilmModel> {
    const url = `${this.filmUrl}/${id}`;
    return this.http.get<FilmModel>(url).pipe(
      tap(_ => this.log(`fetched film id=${id}`)),
      catchError(this.handleError<FilmModel>(`getFilm id=${id}`))
    );
  }

   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */ 
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the eroor to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      // let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  /** Log a FilmService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`FilmService: ${message}`);
  }
}

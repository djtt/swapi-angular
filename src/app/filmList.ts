import { FilmModel } from './filmModel';

export class FilmList {
  count: string;
  next: string;
  previous: string[];
  results: FilmModel[];
}

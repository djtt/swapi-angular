import { PeopleModel } from './peopleModel';

export class PeopleList {
  count: string;
  next: string;
  previous: string[];
  results: PeopleModel[];
}

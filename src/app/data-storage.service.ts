import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { PeopleList } from './peopleList';
import { FilmList } from './filmList';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {
  private peopleList: PeopleList;
  private filmList: FilmList;

  constructor() {}

  public getPeopleList(): PeopleList {
    return this.peopleList;
  }

  public setPeopleList(people: PeopleList): void {
    this.peopleList = people;
  }

  public getFilmList(): FilmList {
    return this.filmList;
  }

  public setFilmList(films: FilmList): void {
    this.filmList = films;
  }

}

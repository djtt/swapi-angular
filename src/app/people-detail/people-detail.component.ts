import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { PeopleService } from '../people.service';
import { DataStorageService } from '../data-storage.service';
import { FilmService } from '../film.service';

import { PeopleModel } from '../peopleModel';
import { PeopleList } from '../peopleList';
import { FilmList } from '../filmList';
import { FilmModel } from '../filmModel';

@Component({
  selector: 'app-people-detail',
  templateUrl: './people-detail.component.html',
  styleUrls: ['./people-detail.component.css']
})
export class PeopleDetailComponent implements OnInit {
  people: PeopleModel;
  peopleList: PeopleList;
  filmList: FilmList;
  film: FilmModel;

  constructor(
    private route: ActivatedRoute, 
    private peopleService: PeopleService, 
    private location: Location, 
    private dataStorageService: DataStorageService,
    private filmService: FilmService) { }

  ngOnInit(): void {
    this.getPeople();
  }

  getPeople(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.peopleList = this.dataStorageService.getPeopleList();
    if (this.peopleList != null) {
      this.people = this.peopleList.results.find(x => x.id == id);
      this.filmBind();
    } else {
      this.filmService.getAllFilms().subscribe(films => {
        this.dataStorageService.setFilmList(films);
	this.peopleService.getPeople(id).subscribe(people => {this.people = people; this.filmBind()});
	});
    }
  }
  filmBind(): void {
    var filmId: number;
    var filmUrls: string[] = this.people.films;
    this.filmList = this.dataStorageService.getFilmList();
    if (this.people.filmsBool != true) {
      for (var i = 0; i < filmUrls.length; i++) {
        filmId = +filmUrls[i].replace("https://test.djtt.nl/api/films/", "").replace("/", "");
        this.film = this.filmList.results.find(x => x.id == filmId);
        filmUrls[i] = this.film.title;
      }
      this.people.filmsBool = true;
    }
  }
  getFilms(): void {
    this.filmService.getAllFilms().subscribe(films => {this.dataStorageService.setFilmList(films)});
  }
  goBack(): void {
    this.location.back();
  }
}

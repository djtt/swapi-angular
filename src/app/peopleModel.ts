export class PeopleModel {
  id: number;
  name: string;
  films: string[];
  filmsBool: boolean;
  gender: string;
  homeworld: string;
  created: string;
  edited: string;
  starships: string[];
  url: string;
  vehicles: string[];
}
